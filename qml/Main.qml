/*
 * Copyright (C) 2023  Alan Moir
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * onlyconnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.12

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'onlyconnect.irnbru'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property bool showHint: false
    property bool sound: true

    Settings {
        id: settings
        property alias showHint: root.showHint
        property alias sound: root.sound
    }

    // background gradient
    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#6287ba" }
            GradientStop { position: 1.0; color: "#1d2e61" }
        }
    }

    PageStack {
        id: stack
        Component.onCompleted: stack.push(Qt.resolvedUrl("MenuPage.qml"))
    }
}
