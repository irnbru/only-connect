import QtQuick 2.7
import Lomiri.Components 1.3
import 'app.js' as App

Rectangle {
    id: timebarContainer

    property alias timeLeft: timebar.isAnimating

    height: units.gu(1)
    width: game.width - units.dp(4)

    radius: units.dp(2)
    color: '#06396e'
    anchors {
        top: game.bottom
        topMargin: units.gu(1)
        right: game.right
        rightMargin: units.dp(2)
    }

    NumberAnimation on scale {
        from: 0
        to: 1
        running: true
        duration: 500
        easing.type: Easing.InCubic
    }

    Rectangle {
        id: timebar
        property alias isAnimating: timebarAnimation.running
        height: parent.height
        width: parent.width
        radius: parent.radius
        color: defaultColor
        anchors.right: parent.right
        NumberAnimation on width {
            id: timebarAnimation
            to: 0
            duration: 150000
            onRunningChanged: {
                if(timebar.width === 0) {
                    // game over
                    timeUp.play()
                    gamePage.remainingLives = 0
                }
            }
        }
    }
}
