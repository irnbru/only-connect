import QtQuick 2.7
import Lomiri.Components 1.3
import 'app.js' as App

LomiriShape {
    property color clr: '#d0e3fc'
    property string txt
    property string txtClr: '#264779'
    property bool wallCompleted

    signal handleClick()

    anchors.fill: parent
    anchors.margins: units.dp(2)
    backgroundColor: clr
    radius: 'small'

    Text {
        text: txt
        anchors.centerIn: parent
        wrapMode: Text.WordWrap
        width: parent.width * 0.9
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: FontUtils.sizeToPixels('medium')
        color: txtClr
        font {
            bold: true
            family: 'ubuntu condensed'
        }
    }

    // to show level completed
    Icon {
        width: parent.width * 0.8
        height: width
        name: 'tick'
        color: txtClr
        anchors.centerIn: parent
        opacity: 0.2
        visible: wallCompleted
    }

    MouseArea {
        anchors.fill: parent
        onClicked: handleClick()
    }
}
