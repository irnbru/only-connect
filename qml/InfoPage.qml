import QtQuick 2.7
import Lomiri.Components 1.3
import 'app.js' as App

Page {
    id: infoPage

    header: PageHeader {
        title: 'About'
        StyleHints {
            backgroundColor: 'transparent'
            foregroundColor: '#fff'
            dividerColor: '#fff'
        }
    }

    Column {
        id: col2
        anchors {
            top: parent.header.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        width: parent.width - units.gu(4)
        spacing: units.gu(1)

        Label {
            text: 'How To Play'
            color: '#fff'
        }

        Label {
            width: parent.width
            wrapMode: Text.WordWrap
            text: 'The aim is to find 4 groups of 4. Each group has a unique connection.'
            textSize: Label.Small
            color: '#fff'
        }

        Label {
            width: parent.width
            wrapMode: Text.WordWrap
            text: 'Click on 4 tiles to see if they are a group.'
            textSize: Label.Small
            color: '#fff'
        }

        Label {
            width: parent.width
            wrapMode: Text.WordWrap
            text: 'You have unlimited guesses to begin with. Once you have found 2 groups, you only have 3 more guesses.'
            textSize: Label.Small
            color: '#fff'
        }

        Label {
            width: parent.width
            wrapMode: Text.WordWrap
            text: 'You have 2:30 to solve the wall. GOOD LUCK!'
            textSize: Label.Small
            color: '#fff'
        }

    }

    Rectangle {
        id: fakeDivider
        width: parent.width
        height: units.dp(1)
        color: '#fff'
        anchors {
            top: col2.bottom
            topMargin: units.gu(2)
        }
    }

    Column {
        id: col
        anchors.top: fakeDivider.bottom
        width: parent.width

        ListItem {
            property bool wanted: false
            height: lilHint.height + divider.height
            divider.colorFrom: '#fff'
            ListItemLayout {
                id: lilHint
                title.text: 'Enable Hints'
                title.color: '#fff'
                subtitle.text: 'Show 1 random group connection for each wall'
                subtitle.color: '#ddd'
                Switch {
                    checked: root.showHint
                    onCheckedChanged: root.showHint = checked
                    SlotsLayout.position: SlotsLayout.Trailing
                }
            }
        }

        ListItem {
            height: lilSound.height + divider.height
            divider.colorFrom: '#fff'
            ListItemLayout {
                id: lilSound
                title.text: 'Play Sound Effects'
                title.color: '#fff'
                subtitle.text: 'Can be toggled in game'
                subtitle.color: '#ddd'
                Switch {
                    checked: root.sound
                    onCheckedChanged: root.sound = checked
                    SlotsLayout.position: SlotsLayout.Trailing
                }
            }
        }

        ListItem {
            height: lilSource.height + divider.height
            divider.colorFrom: '#fff'
            ListItemLayout {
                id: lilSource
                title.text: 'Source Code'
                title.color: '#fff'
                subtitle.text: 'Report bugs or take a sneaky look at the answers'
                subtitle.color: '#ddd'
                ProgressionSlot { color: '#fff' }
            }
            onClicked: Qt.openUrlExternally('https://gitlab.com/irnbru/only-connect')
        }
    }

    Image {
        id: logo
        source: '../assets/logo.svg'
        width: units.gu(45)
        height: width
        anchors {
            top: col.bottom
            topMargin: units.gu(4)
            left: parent.left
            leftMargin: units.gu(-8)
        }
    }
}
