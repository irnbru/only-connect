import QtQuick 2.7
import Lomiri.Components 1.3
import 'app.js' as App

Row {
    id: livesContainer

    property bool life1visible: false
    property bool life2visible: false
    property bool life3visible: false

    width: game.width * 0.25
    height: units.gu(3)
    spacing: units.dp(4)
    anchors {
        left: infoContainer.left
        leftMargin: units.dp(2)
        bottom: infoContainer.bottom
        bottomMargin: units.gu(1)
    }

    Image {
        id: life1
        width: parent.width * 0.3
        height: width
        source: '../assets/logo.svg'
        scale: life1visible ? 1 : 0
        Behavior on scale {
            NumberAnimation { duration: 500; easing.type: Easing.InCubic }
        }
    }

    Image {
        id: life2
        width: parent.width * 0.3
        height: width
        source: '../assets/logo.svg'
        scale: life2visible ? 1 : 0
        Behavior on scale {
            NumberAnimation { duration: 500; easing.type: Easing.InCubic }
        }
    }

    Image {
        id: life3
        width: parent.width * 0.3
        height: width
        source: '../assets/logo.svg'
        scale: life3visible ? 1 : 0
        Behavior on scale {
            NumberAnimation { duration: 500; easing.type: Easing.InCubic }
        }
    }
}
