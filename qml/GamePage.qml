import QtQuick 2.7
import QtMultimedia 5.6
import Lomiri.Components 1.3
import 'app.js' as App

Page {
    id: gamePage

    property int menuPageIndex
    property int menuGridIndex
    property int menuGameNumber
    property ListModel menuModel

    property int completedGroups: 0
    property string defaultColor: '#d0e3fc'
    property string textDark: '#264779'
    property string textLight: '#ffffff'
    property var groupColors: ['#06396e', '#21886a', '#610038', '#0d8294']
    property string groupColor
    property int remainingLives: 3

    property string hintText

    onCompletedGroupsChanged: groupColor = groupColors[completedGroups]

    onRemainingLivesChanged: {
        App.playSound(loseLife)
        //loseLife.play()
        if(remainingLives === 2) lives.life3visible = false
        if(remainingLives === 1) lives.life2visible = false
        if(remainingLives === 0) {
            lives.life1visible = false
            hint.scale = 0.0 // animate the hint out before resolveBtn appears
        }
    }

    header: PageHeader {
        title: 'Wall ' + (menuGameNumber + 1)
        trailingActionBar.actions: [
            Action {
                iconName: settings.sound ? "audio-speakers-symbolic" : "audio-speakers-muted-symbolic"
                onTriggered: root.sound = !root.sound
            }
        ]
        StyleHints {
            backgroundColor: 'transparent'
            foregroundColor: '#fff'
            dividerColor: '#fff'
        }
    }

    ListModel { id: gameModel }
    ListModel { id: connectionsModel }
    ListModel { id: currentTiles }


    // Audio {
    //     id: buttonClick
    //     source: '../assets/wallBtnClick.mp3'
    // }
    //
    // Audio {
    //     id: loseLife
    //     source: '../assets/loseLife.mp3'
    // }
    //
    // Audio {
    //     id: ocFlurry
    //     source: '../assets/ocFlurry.mp3'
    // }
    //
    // Audio {
    //     id: solveClue
    //     source: '../assets/solveClue.mp3'
    // }
    //
    // Audio {
    //     id: timeUp
    //     source: '../assets/timeUp.mp3'
    // }

    SoundEffect {
        id: buttonClick
        source: '../assets/wallBtnClick.wav'
    }
    SoundEffect {
        id: loseLife
        source: '../assets/loseLife.wav'
    }
    SoundEffect {
        id: ocFlurry
        source: '../assets/ocFlurry.wav'
    }
    SoundEffect {
        id: solveClue
        source: '../assets/solveClue.wav'
    }
    SoundEffect {
        id: timeUp
        source: '../assets/timeUp.wav'
    }

    Item {
        id: infoContainer
        anchors {
            top: parent.header.bottom
            bottom: game.top
            left: game.left
            right: game.right
        }

        Text {
            id: hint
            visible: root.showHint
            text: "Connection: " + hintText
            color: '#fff'
            font {
                bold: true
            }
            anchors.centerIn: parent
            width: game.width
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            scale: 1.0
            Behavior on scale {
                NumberAnimation { duration: 500; easing.type: Easing.InCubic }
            }
        }

        // Text shown on completing a wall
        Text {
            id: winner
            text: "You solved the wall !"
            color: '#fff'
            font {
                bold: true
            }
            anchors.centerIn: parent
            width: game.width
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            scale: 0.0
            Behavior on scale {
                NumberAnimation { duration: 500; easing.type: Easing.InCubic }
            }
        }

        Button {
            id: resolveBtn
            enabled: true
            visible: remainingLives === 0
            color: defaultColor
            width: childText.width + units.gu(2)
            scale: visible ? 1 : 0
            Behavior on scale {
                NumberAnimation { duration: 500; easing.type: Easing.InCubic }
            }
            Text {
                id: childText
                anchors.centerIn: parent
                text: 'Resolve the wall'
                color: '#264779'
                font {
                    bold: true
                    family: 'ubuntu condensed'
                }
            }
            anchors.centerIn: parent
            onClicked: App.resolveWall()
        }

        Lives { id: lives }
    }

    GridView {
        id: game
        interactive: false
        width: units.gu(41)
        height: width * 0.8
        anchors.centerIn: parent
        anchors.topMargin: gamePage.header.height
        cellWidth: game.width / 4
        cellHeight: game.height / 4
        add: Transition {
            NumberAnimation {
                properties: 'scale'
                from: 0
                to: 1
                duration: 500
                easing.type: Easing.InCubic
            }
        }
        move: Transition {
            NumberAnimation {
                properties: 'x, y'
                duration: 500
                easing.type: Easing.InCubic
            }
        }
        moveDisplaced: Transition {
            NumberAnimation {
                properties: 'x, y'
                duration: 500
                easing.type: Easing.InCubic
            }
        }
        model: gameModel
        delegate: Rectangle {
            width: game.cellWidth
            height: game.cellHeight
            color: 'transparent'
            GridBtn {
                txt: content
                clr: bgColor
                txtClr: txtColor
                anchors.centerIn: parent
                onHandleClick: {
                    if(timebar.timeLeft) {
                        if(!done) {
                            App.handleTileClick(index, menuPageIndex, menuGridIndex, menuModel, menuGameNumber)
                        }
                    }
                }
            }
        }
    }

    Timebar { id: timebar }

    // show connections once wall is solved or user clicks 'show connecitons' button
    Item {
        id: connectionsContainer
        anchors {
            top: timebar.bottom
            bottom: gamePage.bottom
            bottomMargin: units.gu(1)
            left: game.left
            right: game.right
        }

        ListView {
            id: connectionsView
            height: contentHeight
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: units.dp(2) // to line up with GridBtn which have a margin of units.dp(2)
            }
            spacing: units.gu(1)
            interactive: false
            model: connectionsModel
            delegate: Text {
                id: txt
                //'add' or 'populate' wont work for some reason so janky workaround it is
                property bool entering: false

                text: (index + 1) + '. ' + connection
                wrapMode: Text.WordWrap
                color: '#ffffff'
                font.pixelSize: FontUtils.sizeToPixels('medium')
                font.bold: true
                opacity: 0
                NumberAnimation on opacity {
                    id: anim
                    to: 1
                    duration: 500
                    easing.type: Easing.InCubic
                    running: entering
                }
                Timer {
                    id: staggerer
                    interval: 200 * index
                    onTriggered: txt.entering = true
                }
                Component.onCompleted: staggerer.start()
            }
        }
    }

    Component.onCompleted: {
        App.createWall(menuGameNumber)
        groupColor = groupColors[completedGroups]
        hintText = gameModel.get(0).connection
        App.playSound(solveClue)
    }
}
