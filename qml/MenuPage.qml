import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.12
import 'app.js' as App

Page {
    id: menuPage

    header: PageHeader {
        title: "Only Connect"
        trailingActionBar.actions: [
            Action {
                iconName: "info"
                onTriggered: stack.push(Qt.resolvedUrl("InfoPage.qml"))
            },
            Action {
                iconName: "delete"
                onTriggered: PopupUtils.open(dialog)
            }
        ]
        StyleHints {
            backgroundColor: "transparent"
            foregroundColor: "#fff"
            dividerColor: "#fff"
        }
    }

    ListModel { id: menuModel }

    // Swipable pages of grids
    ListView {
        id: menuView
        anchors {
            left: menuPage.left
            right: menuPage.right
            top: menuPage.top
            bottom: menuPage.bottom
        }
        clip: true
        orientation: ListView.Horizontal
        model: menuModel
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 500
        delegate: Rectangle {
            property int pageIndex: index
            height: menuView.height
            width: menuView.width
            color: "transparent"

            GridView {
                id: pageGrid
                interactive: false
                width: units.gu(41)
                height: width * 0.8
                anchors.centerIn: parent
                cellWidth: pageGrid.width / 4
                cellHeight: pageGrid.height / 4
                model: menuModel.get(index).gridNumbers
                delegate: Rectangle {
                    width: pageGrid.cellWidth
                    height: pageGrid.cellHeight
                    color: "transparent"

                    GridBtn {
                        txt: gridNumber + 1
                        wallCompleted: completed
                        anchors.centerIn: parent
                        onHandleClick: stack.push(Qt.resolvedUrl("GamePage.qml"), {
                            menuPageIndex: pageIndex,
                            menuGridIndex: index, // always between 0 and 15
                            menuGameNumber: gridNumber, // all numbers from 0 to last game
                            menuModel
                        })
                    }
                }
            }
        }
        Component.onCompleted: App.createMenu()
    }

    // Indicators to show which page you are on
    ListModel { id: indicatorModel }

    ListView {
        id: indicatorView
        anchors {
            bottom: menuPage.bottom
            bottomMargin: units.gu(4)
            horizontalCenter: menuPage.horizontalCenter
        }
        width: contentWidth
        currentIndex: menuView.currentIndex
        orientation: ListView.Horizontal
        model: indicatorModel
        delegate: Rectangle {
            width: units.gu(3)
            height: width
            color: "transparent"

            Rectangle {
                width: units.gu(1)
                height: width
                radius: width
                color: "#d0e3fc"
                //color: "#fff"
                opacity: 0.3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: menuView.currentIndex = index
            }
        }

        highlight: Rectangle {
            width: units.gu(3)
            height: width
            color: "transparent"

            Rectangle {
                width: units.gu(1)
                height: width
                radius: width
                color: "#d0e3fc"
                anchors.centerIn: parent
            }
        }
    }

    // delete saved progress dialog
    Component {
        id: dialog
        Dialog {
            id: dialogue
            title: "Delete Progress"
            text: "Are you sure you want to delete all saved progress ?"
            Button {
                text: "cancel"
                onClicked: PopupUtils.close(dialogue)
            }
            Button {
                text: "delete all"
                color: theme.palette.normal.negative
                onClicked: {
                    App.deleteProgress()
                    PopupUtils.close(dialogue)
                }
            }
        }
    }
}
