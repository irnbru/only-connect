// database functions

function getDatabase() {
    return LocalStorage.openDatabaseSync("OnlyConnectDb", "1.0", "Completedlevels", 100000)
}

function createTable() {
    var db = getDatabase()
    db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS completed (pageNumber INT, pageGridIndex INT, gameNumber INT)")
    })
}

function saveGame(pageNumber, pageGridIndex, gameNumber) {
    var db = getDatabase()
    db.transaction(function(tx) {
        tx.executeSql("INSERT INTO completed (pageNumber, pageGridIndex, gameNumber) VALUES (?, ?, ?);", [pageNumber, pageGridIndex, gameNumber])
    })
}

// get all completed games and show in menu
function getSavedGames() {
    var db = getDatabase()
    createTable()
    var rs = ""
    db.transaction(function(tx) {
        rs = tx.executeSql("SELECT * FROM completed")

        if(rs.rows.length > 0) {
            // update menu with saved games
            for(var i = 0; i < rs.rows.length; i++) {
                menuModel.get(rs.rows.item(i).pageNumber).gridNumbers.get(rs.rows.item(i).pageGridIndex).completed = true
            }
        }
    })
}

// check if we need to save completed game ( could be already completed )
function checkForSavedGame(gameNum) {
    var db = getDatabase()
    var rs = ""
    db.transaction(function(tx) {
        rs = tx.executeSql("SELECT * FROM completed WHERE gameNumber = '" + gameNum + "'")
        if(rs.rows.length > 0) {
            return true
        }
    })

    return false
}

// delete all saved games from db
function deleteProgress() {
    var db = getDatabase()
    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM completed")
    })
    menuModel.clear()
    createMenu()
}

// menu creation

function createMenu() {
    // create 'pages' for menu
    for(var i = 0; i < walls.length; i += 16) {
        menuModel.append({ pageNumber: i, gridNumbers: [] })
    }

    // create menu grids for each page
    for(var j = 0; j < menuModel.count; j++) {
        for(var k = (16 * j); k < (16 * j) + 16; k++) {
            if(k < walls.length) {
                menuModel.get(j).gridNumbers.append({ gridNumber: k, completed: false })
            }
        }
    }

    // check db for completed walls
    getSavedGames()

    indicatorModel.clear()
    createIndicators()
}

// create dots to represent page number
function createIndicators() {
    for(var i = 0; i < menuModel.count; i++) {
        indicatorModel.append({ indicator: i })
    }
}

// randomize each game so they are not the same every time
function shuffleArray(arr) {
    var i = arr.length, k, temp
    while(--i > 0) {
        k = Math.floor(Math.random() * (i + 1))
        temp = arr[k]
        arr[k] = arr[i]
        arr[i] = temp
    }

    return arr
}

// create the game wall
function createWall(ind) {
    var wall = walls[ind]
    var keys = Object.keys(wall)
    var arr = []

    for(var i = 0; i < keys.length; i++) {
        var options = wall[keys[i]].options
        for(var j = 0; j < options.length; j++) {
            arr.push({
                content: options[j],
                connection: wall[keys[i]].connection
            })
        }
    }

    var shuffled = shuffleArray(arr)

    shuffled.forEach(function(group) {
        gameModel.append({
            content: group.content,
            connection: group.connection,
            bgColor: defaultColor,
            txtColor: textDark,
            selected: false,
            done: false
        })
    })
}

function handleTileClick(ind, menuPageIndex, menuGridIndex, menuModel, menuGameNumber) {
    //buttonClick.play()
    playSound(buttonClick)

    if(gameModel.get(ind).selected) {
        // unselect tile and remove from currentTiles
        gameModel.set(ind, { selected: false, bgColor: defaultColor, txtColor: textDark })
        for(var i = 0; i < currentTiles.count; i++) {
            if(currentTiles.get(i).tileInd === ind) {
                currentTiles.remove(i)
            }
        }
    } else {
        gameModel.set(ind, { selected: true, bgColor: groupColor, txtColor: textLight })
        currentTiles.append({ tileInd: ind })
        if(currentTiles.count === 4) {
            if(checkConnection()) {
                alignGroup(menuPageIndex, menuGridIndex, menuModel, menuGameNumber)
            } else {
                resetTiles()
            }
        }
    }
}

function checkConnection() {
    var condition = gameModel.get(currentTiles.get(0).tileInd).connection

    for(var i = 0; i < currentTiles.count; i++) {
        if(condition !== gameModel.get(currentTiles.get(i).tileInd).connection) {
            return false
        }
    }

    return true
}

function alignGroup(menuPageIndex, menuGridIndex, menuModel, menuGameNumber) {
    //solveClue.play()
    playSound(solveClue)

    var arr = []

    // if the correct connection === the hint text, remove the hint text
    if(hint.visible) {
        if(hintText === gameModel.get(currentTiles.get(0).tileInd).connection) {
            hint.scale = 0.0
            //hint.visible = false
        }
    }

    for(var i = 0; i < currentTiles.count; i++) {
        arr.push(currentTiles.get(i).tileInd)
    }

    // sort the tiles to avoid janky animation in gameView move
    var sorted = arr.sort(function(a, b) { return a - b })

    for(var j = 0; j < sorted.length; j++) {
        // done prevent tile click after a group is completed
        gameModel.set(sorted[j], { done: true })
        gameModel.move(sorted[j], j + (completedGroups * 4), 1)
    }

    currentTiles.clear()
    completedGroups++

    if(completedGroups === 2) {
        showAllLives()
    }

    if(completedGroups === 3) {
        wallCompleted(menuPageIndex, menuGridIndex, menuModel, menuGameNumber)
    }
}

function resetTiles() {
    for(var i = 0; i < currentTiles.count; i++) {
        gameModel.set(currentTiles.get(i).tileInd, { bgColor: defaultColor, txtColor: textDark, selected: false })
    }

    currentTiles.clear()

    if(completedGroups > 1) {
        remainingLives--
        if(remainingLives === 0) {
            timebar.timeLeft = false
        }
    }
}

function wallCompleted(menuPageIndex, menuGridIndex, menuModel, menuGameNumber) {
    //ocFlurry.play()
    playSound(ocFlurry)
    timebar.timeLeft = false

    // color remaining tiles, already in correct position
    var arr = [12, 13, 14, 15]
    for(var i = 0; i < arr.length; i++) {
        gameModel.set(arr[i], { bgColor: groupColor, txtColor: textLight, done: true })
    }

    winner.scale = 1.0
    showConnections()
    hint.scale = 0.0

    // game has not been completed already, so save it
    if(!checkForSavedGame(menuGameNumber)) {
        saveGame(menuPageIndex, menuGridIndex, menuGameNumber)
    }

    // show that wall has been completed in the menu ( with a tick or something ), doesnt show until restarting the app
    menuModel.get(menuPageIndex).gridNumbers.get(menuGridIndex).completed = true
}

// If the user wants to see the solution
function resolveWall() {
    playSound(solveClue)
    //solveClue.play()
    resolveBtn.enabled = false
    var startIndex = completedGroups * 4
    var firstGroupConnection = gameModel.get(startIndex).connection
    var arr = []

    // get the first uncompleted group
    for(var i = startIndex; i < gameModel.count; i++) {
        if(gameModel.get(i).connection === firstGroupConnection) {
            arr.push(i)
        }
    }

    var sorted = arr.sort(function(a, b) { return a - b })

    for(var j = 0; j < sorted.length; j++) {
        gameModel.set(sorted[j], { done: true, bgColor: groupColor, txtColor: textLight })
        gameModel.move(sorted[j], j + (completedGroups * 4), 1)
    }

    if(completedGroups < 3) {
        completedGroups++
        arr = []
        resolveWall()
        return
    }

    showConnections()
}

function showConnections() {
    for(var i = 0; i < gameModel.count; i += 4) {
        connectionsModel.append({ connection: gameModel.get(i).connection })
    }
}

// lives remaining are shown by 3 small logo images in game
function showAllLives() {
    lives.life1visible = true
    lives.life2visible = true
    lives.life3visible = true
}

function playSound(soundId) {
    if(settings.sound) {
        // this is for the btn click -- clicking too quickly doesn't always trigger the sound unless
        // we stop it then play it
        soundId.stop()
        soundId.play()
    }

}

var walls = [
  // ==================== series 1 =========================
  {
    group_1: {connection: 'Made from rubber', options: ['Elastic band', 'Puck', 'Squash ball', 'Condom']},
    group_2: {connection: 'Prison slang', options: ['Screw', 'Grass', 'Snout', 'Lag']},
    group_3: {connection: 'Called "Nick"', options: ['Santa', 'Satan', 'Bottom', 'Dr Riviera']},
    group_4: {connection: 'Quarks', options: ['Up', 'Down', 'Charm', 'Strange']},
    // series: 'Series 1'
  },
  {
    group_1: {connection: 'Terms for "zero"', options: ['Love', 'Zip', 'Goose-egg', 'Duck']},
    group_2: {connection: 'Fictional captains', options: ['Sparrow', 'Hook', 'Haddock', 'Ahab']},
    group_3: {connection: 'Playing card nicknames', options: ['Bullet', 'Cowboy', 'Deuce', 'Crab']},
    group_4: {connection: 'Flying __', options: ['Fox', 'Ace', 'Fish', 'Buttress']},
    // series: 'Series 1'
  },
  {
    group_1: {connection: 'English cricket captains', options: ['May', 'Gower', 'Lamb', 'Fry']},
    group_2: {connection: 'Trees', options: ['Cork', 'Ash', 'Lime', 'Willow']},
    group_3: {connection: 'US presidential losers', options: ['Carter', 'Dole', 'Gore', 'Bush']},
    group_4: {connection: 'Irish counties', options: ['Galway', 'Mayo', 'Clare', 'Kerry']},
    //: 'Series 1'
  },
  {
    group_1: {connection: '__ water', options: ['Tonic', 'Heavy', 'Bath', 'Barley']},
    group_2: {connection: 'Chancellors of the Exchequer', options: ['Major', 'Clarke', 'Darling', 'Brown']},
    group_3: {connection: 'Morris cars', options: ['Marina', 'Minor', 'Oxford', 'Isis']},
    group_4: {connection: 'PC', options: ['Computer', 'Constable', '%', 'Postcard']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Irish counties', options: ['Cork', 'Wexford', 'Longford', 'Clare']},
    group_2: {connection: 'TV doctors', options: ['Martin', 'Quinn', 'Kildare', 'House']},
    group_3: {connection: 'Trees', options: ['Hazel', 'Quince', 'Rowan', 'Myrtle']},
    group_4: {connection: 'Houses', options: ['Somerset', 'Opera', 'Wendy', 'Heartbreak']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Fictional detectives', options: ['Rebus', 'Hammer', 'Wimsey', 'Spade']},
    group_2: {connection: '__ puzzle', options: ['Monkey', 'Chinese', 'Jigsaw', 'Crossword']},
    group_3: {connection: 'Fabrics', options: ['Terry', 'Drill', 'Wincey', 'Jersey']},
    group_4: {connection: 'Tools', options: ['Trowel', 'Pick', 'Riddle', 'Wimble']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Wombles', options: ['Alderney', 'Bulgaria', 'Tobermory', 'Orinoco']},
    group_2: {connection: '"New" US States', options: ['Jersey', 'York', 'Hampshire', 'Mexico']},
    group_3: {connection: 'Types of fever', options: ['Glandular', 'Scarlet', 'Trench', 'Yellow']},
    group_4: {connection: 'Beef __', options: ['Tea', 'Wellington', 'Jerky', 'Stroganoff']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'London Underground lines', options: ['Northern', 'District', 'Circle', 'Jubilee']},
    group_2: {connection: 'Horse riding styles', options: ['Western', 'English', 'Bareback', 'Jumping']},
    group_3: {connection: '__ cross', options: ['Southern', 'Maltese', 'Victoria', 'Red']},
    group_4: {connection: 'USA time zones', options: ['Eastern', 'Central', 'Mountain', 'Pacific']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Famous Mels', options: ['Blanc', 'Brooks', 'C', 'Smith']},
    group_2: {connection: 'Household tools', options: ['Screwdriver', 'Hammer', 'Gimlet', 'Wrench']},
    group_3: {connection: 'Cocktails', options: ['Sidecar', 'Manhattan', 'Gibson', 'Margarita']},
    group_4: {connection: 'Monkey __', options: ['Puzzle', 'Business', 'Nuts', 'Suit']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Cakes', options: ['Angel', 'Fairy', 'Marble', 'Madeleine']},
    group_2: {connection: 'Poets', options: ['Spenser', 'Lawrence', 'Pope', 'Pound']},
    group_3: {connection: 'Fictional detectives', options: ['Queen', 'Marlowe', 'Hammer', 'Holmes']},
    group_4: {connection: 'Waterfalls', options: ['Victoria', 'Churchill', 'Horseshoe', 'Reichenbach']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Thames bridges', options: ['Lambeth', 'Queen Elizabeth II', 'Millenium', 'London']},
    group_2: {connection: 'Crabs', options: ['Spider', 'Shore', 'Fiddler', 'Velvet']},
    group_3: {connection: 'Blue __', options: ['Bell', 'Print', 'Stocking', 'Bottle']},
    group_4: {connection: 'Tarot cards', options: ['Chariot', 'Moon', 'Hermit', 'Tower']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Slang for money', options: ['Dough', 'Lolly', 'Dosh', 'Readies']},
    group_2: {connection: 'Birds', options: ['Crane', 'Swift', 'Turkey', 'Magpie']},
    group_3: {connection: 'Fruit', options: ['Passion', 'Bread', 'Kiwi', 'Star']},
    group_4: {connection: 'TV sitcoms', options: ['Taxi', 'Brass', 'Bottom', 'Porridge']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Snakes', options: ['Coral', 'Adder', 'Sidewinder', 'Grass']},
    group_2: {connection: 'Chaucer characters', options: ['Reeve', 'Cook', 'Knight', 'Miller']},
    group_3: {connection: 'Playwrights', options: ['Cooney', 'Hare', 'Coward', 'Congreve']},
    group_4: {connection: 'Missiles', options: ['Hawk', 'Cruise', 'Trident', 'Patriot']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Cluedo characters', options: ['White', 'Mustard', 'Plum', 'Peacock']},
    group_2: {connection: 'Early 20th C comedians', options: ['Wall', 'Fields', 'Hope', 'Hay']},
    group_3: {connection: 'Golfers', options: ['Woods', 'Rose', 'Strange', 'Watson']},
    group_4: {connection: '__ fever', options: ['Gold', 'Yellow', 'Jungle', 'Scarlet']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Ear parts', options: ['Stirrup', 'Tympanum', 'Anvil', 'Hammer']},
    group_2: {connection: 'Types of numbers', options: ['Perfect', 'Square', 'Cube', 'Irrational']},
    group_3: {connection: 'Musical directions', options: ['Presto', 'Piano', 'Largo', 'Grave']},
    group_4: {connection: 'Anagrams of each other', options: ['Altering', 'Triangle', 'Relating', 'Integral']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Parts of a staircase', options: ['Newel', 'Bannister', 'Tread', 'Riser']},
    group_2: {connection: '__ cake', options: ['Jaffa', 'Eccles', 'Banbury', 'Chorley']},
    group_3: {connection: 'Quiz show hosts', options: ['Forsyth', 'Edmonds', 'Holmes', 'Parsons']},
    group_4: {connection: 'Fictional professors', options: ['Plum', 'Moriarty', 'Indiana Jones', 'Higgins']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Names for a birds mandible', options: ['Rostrum', 'Nib', 'Beak', 'Bill']},
    group_2: {connection: 'Nicknames for professionals', options: ['Shrink', 'Quack', 'Spark', 'Hack']},
    group_3: {connection: 'Johnny Depp roles', options: ['Sweeny Todd', 'J.M. Barrie', 'Roux', 'Willy Wonka']},
    group_4: {connection: 'Magicians actions', options: ['Force', 'Crimp', 'Palm', 'Cop']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Sound like letters of the alphabet', options: ['Queue', 'Pea', 'Jay', 'Sea']},
    group_2: {connection: 'Famous Jacks', options: ['Russel', 'Straw', 'Frost', 'Dee']},
    group_3: {connection: 'Shades of green', options: ['Lime', 'Emerald', 'Jade', 'Olive']},
    group_4: {connection: '__ Brothers', options: ['Warner', 'Wright', 'Chemical', 'Moss']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Sundays', options: ['Rogation', 'Trinity', 'Bloody', 'Palm']},
    group_2: {connection: 'Ships', options: ['Bounty', 'Beagle', 'Pinta', 'Victory']},
    group_3: {connection: 'Irish __', options: ['Coffee', 'Terrier', 'Yew', 'Rover']},
    group_4: {connection: 'Rebellions', options: ['Boxer', 'Whiskey', 'Mau Mau', 'Easter']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Jazz pianists', options: ['Hancock', 'Monk', 'Tatum', 'Waller']},
    group_2: {connection: 'Shades of red', options: ['Rust', 'Cardinal', 'Burgundy', 'Venetian']},
    group_3: {connection: 'Types of collar', options: ['Mandarin', 'Eton', 'Butterfly', 'Windsor']},
    group_4: {connection: 'Types of gin', options: ['Old Tom', 'London', 'Plymouth', 'Holland']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Biscuits', options: ['Bourbon', 'Garibaldi', 'Lincoln', 'Abernethy']},
    group_2: {connection: 'British literary awards', options: ['Angus', 'Costa', 'Orwell', 'Orange']},
    group_3: {connection: 'Ruling houses of Europe', options: ['Belgium', 'Grimaldi', 'Windsor', 'Bernadotte']},
    group_4: {connection: 'Scottish castles', options: ['Glamis', 'Urquhart', 'Balmoral', 'Inverary']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Star Trek characters', options: ['Chekov', 'Kirk', 'McCoy', 'Scott']},
    group_2: {connection: 'Antarctic explorers', options: ['Ross', 'Shackleton', 'Weddell', 'Fuchs']},
    group_3: {connection: 'Seas in the Pacific ocean', options: ['Sulu', 'Yellow', 'Bismarck', 'Solomon']},
    group_4: {connection: 'UK art galleries', options: ['Dean', 'Walker', 'Baltic', 'Whitworth']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Models of car', options: ['Golf', 'Sierra', 'Victor', 'Shogun']},
    group_2: {connection: 'Spacecraft', options: ['Echo', 'Explorer', 'Voyager', 'Sputnik']},
    group_3: {connection: 'Dances', options: ['Foxtrot', 'Tango', 'Tap', 'Morris']},
    group_4: {connection: 'Michael Caine films', options: ['Zulu', 'Water', 'Mona Lisa', 'Ashanti']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Types of shoe', options: ['Wedge', 'Pump', 'Mule', 'Brogue']},
    group_2: {connection: 'Named after famous people', options: ['Wellington', 'Sandwich', 'Plimsoll', 'Pavlova']},
    group_3: {connection: 'TV cats', options: ['Custard', 'Vienna', 'Snowball', 'Cookie']},
    group_4: {connection: 'Animal Farm characters', options: ['Clover', 'Napoleon', 'Major', 'Boxer']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'One word pop stars', options: ['Seal', 'Sting', 'Jewel', 'Pink']},
    group_2: {connection: 'Pantomime characters', options: ['Goose', 'Dame', 'Baron', 'Prince']},
    group_3: {connection: 'Jazz musician nicknames', options: ['Bird', 'Count', 'Duke', 'King']},
    group_4: {connection: 'Chocolate __', options: ['Soldier', 'Sauce', 'Biscuits', 'Buttons']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Items in a Christmas pudding', options: ['Suet', 'Flour', 'Sixpence', 'Peel']},
    group_2: {connection: 'Characters in Great Expectations', options: ['Drummle', 'Biddy', 'Estella', 'Pip']},
    group_3: {connection: 'Palindromes', options: ['Hannah', 'Naan', 'Level', 'Peep']},
    group_4: {connection: 'Varieties of bee', options: ['Honey', 'Mason', 'Bumble', 'Mining']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Members of a rowing eight', options: ['Stroke', 'Engine room', 'Cox', 'Bowman']},
    group_2: {connection: 'Famous architects', options: ['Wren', 'Piano', 'Barry', 'Scott']},
    group_3: {connection: 'Female Radio 1 DJs', options: ['Long', 'Mac', 'Ball', 'Nightingale']},
    group_4: {connection: '__ bar', options: ['Cross', 'Sushi', 'Handle', 'Crow']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Heritage railway lines', options: ['Lavender', 'Wensleydale', 'Watercress', 'Bluebell']},
    group_2: {connection: 'Witch __', options: ['Doctor', 'Hazel', 'Hunt', 'Craft']},
    group_3: {connection: 'Types of pie', options: ['Mud', 'Blackberry', 'Rabbit', 'Eskimo']},
    group_4: {connection: 'Official nicknames of US states', options: ['Grand Canyon', 'Keystone', 'Bluegrass', 'Silver']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'US Presidents', options: ['HST', 'FDR', 'GWB', 'LBJ']},
    group_2: {connection: 'Pop bands', options: ['ABC', 'XTC', 'EMF', 'TLC']},
    group_3: {connection: 'Qualifications', options: ['HNC', 'SVQ', 'MBA', 'NVQ']},
    group_4: {connection: 'TV channels', options: ['NBC', 'RAI', 'QVC', 'TNT']},
    //: 'Series 1'
  },
  {
    group_1: {connection: 'Perfect numbers', options: ['6', '28', '496', '8128']},
    group_2: {connection: 'Films', options: ['Seven', '8 1/2', '1984', '2010']},
    group_3: {connection: 'Temperatures of waters freezing point', options: ['0', '32', '273.15', '491.67']},
    group_4: {connection: 'Maximum scores in games', options: ['21', '147', '180', '300']},
    //: 'Series 1'
  },
  // ==================== series 2 =========================
  {
    group_1: {connection: 'Alfred Hitchcock films', options: ['Topaz', 'Rose', 'Blackmail', 'Spellbound']},
    group_2: {connection: 'Sporting arenas', options: ['Diamond', 'Oval', 'Ring', 'Court']},
    group_3: {connection: 'Famous assassins', options: ['Ruby', 'Chapman', 'Booth', 'Ray']},
    group_4: {connection: 'Pirates in "Treasure Island"', options: ['Pew', 'Bones', 'Silver', 'Hands']},
    //: 'Series 2'
  },
  {
    group_1: {connection: '"Doctor Who" companions', options: ['Rose', 'Sarah Jane', 'Tegan', 'Ace']},
    group_2: {connection: 'Iron __', options: ['Cross', 'Fist', 'Lady', 'Duke']},
    group_3: {connection: 'London theaters', options: ['Globe', 'Apollo', 'Duchess', 'Adelphi']},
    group_4: {connection: 'Kicks in martial arts', options: ['Roundhouse', 'Crescent', 'Butterfly', 'Scissor']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Names for marijuana', options: ['Hay', 'Green', 'Tea', 'Grass']},
    group_2: {connection: 'Jack __', options: ['Rabbit', 'Knife', 'Hammer', 'Ass']},
    group_3: {connection: 'Philosophers', options: ['Berlin', 'Hume', 'Mill', 'Locke']},
    group_4: {connection: 'Things you can "fire"', options: ['Question', 'Pot', 'Gun', 'Employee']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Names of magazines', options: ['Arena', 'Time', 'Vogue', 'Prospect']},
    group_2: {connection: 'Burning __', options: ['Issue', 'Mountain', 'Bush', 'Glass']},
    group_3: {connection: 'US Vice Presidents', options: ['Fairbanks', 'Rockefeller', 'Agnew', 'Humphrey']},
    group_4: {connection: 'British film directors', options: ['Reed', 'Lean', 'Forbes', 'Powell']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Styles of shoe heel', options: ['Wedge', 'Cuban', 'Kitten', 'Platform']},
    group_2: {connection: 'Young creatures', options: ['Fry', 'Elver', 'Joey', 'Lamb']},
    group_3: {connection: 'Doctors in ER', options: ['Carter', 'Ross', 'Greene', 'Benton']},
    group_4: {connection: 'Surnames derived from craftsmen', options: ['Goldsmith', 'Chandler', 'Fletcher', 'Cartwright']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Famous Hungarians', options: ['Biro', 'Bartok', 'Rubik', 'Houdini']},
    group_2: {connection: 'Moustaches', options: ['Dali', 'Walrus', 'Fu Manchu', 'Hitler']},
    group_3: {connection: 'Styles of skirt', options: ['Pencil', 'Bubble', 'Tulip', 'Hobble']},
    group_4: {connection: 'Pig __', options: ['Headed', 'Pen', 'Tail', 'Skin']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Muppets', options: ['Waldorf', 'Beaker', 'Dr Teeth', 'Scooter']},
    group_2: {connection: 'Haircuts', options: ['Caesar', 'Buzz', 'Pudding basin', 'Crew']},
    group_3: {connection: 'Trophies in rugby', options: ['Nelson Mandela', 'Webb Ellis', 'Calcutta', 'Cook']},
    group_4: {connection: 'Jazz trumpeters', options: ['Davis', 'Gillespie', 'Armstrong', 'Baker']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Pigeons', options: ['Passanger', 'Rock', 'Wood', 'Homing']},
    group_2: {connection: 'Beard styles', options: ['Spade', 'Chinstrap', 'Van Dyck', 'Soul patch']},
    group_3: {connection: 'Warships', options: ['Monitor', 'Galley', 'Carrier', 'Corvette']},
    group_4: {connection: 'Types of bicycles', options: ['Beach cruiser', 'Mountain', 'Tandem', 'Racing']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Hebrew', options: ['He', 'Gimel', 'Beth', 'Nun']},
    group_2: {connection: 'Golf shots', options: ['Flop', 'Chip', 'Shank', 'Drive']},
    group_3: {connection: 'Ways to tease', options: ['Chaff', 'Rib', 'Kid', 'Rag']},
    group_4: {connection: 'End in boys names', options: ['Fluke', 'Divan', 'Trick', 'Truss']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Soups', options: ['Mock turtle', 'Alphabet', 'Palestine', 'Pea']},
    group_2: {connection: 'Places called "__ Springs"', options: ['Alice', 'Buffalo', 'Palm', 'Colorado']},
    group_3: {connection: 'Anagrams of other clues', options: ['Lamp', 'Ape', 'Shrub', 'Celia']},
    group_4: {connection: 'As mad as a __', options: ['March Hare', 'Brush', 'Wet hen', 'Bag of hammers']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Islands in New York Harbour', options: ['Liberty', 'Ellis', 'Rikers', 'Long']},
    group_2: {connection: 'Blue Peter presenters', options: ['Groom', 'Curry', 'Bacon', 'Salmon']},
    group_3: {connection: 'Terms in set theory', options: ['Class', 'Complement', 'Union', 'Singleton']},
    group_4: {connection: 'Types of pink', options: ['Persian', 'Coral', 'Carnation', 'Rose']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Foreign secretaries', options: ['Cook', 'Beckett', 'Straw', 'Owen']},
    group_2: {connection: 'Aviators', options: ['Johnson', 'Post', 'Wright', 'Hughes']},
    group_3: {connection: 'Moths', options: ['Miller', 'Gypsy', 'Winter', 'Hawk']},
    group_4: {connection: 'The Last __', options: ['Ditch', 'Trump', 'Emperor', 'Supper']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Professional snooker players', options: ['Fu', 'Virgo', 'White', 'Perry']},
    group_2: {connection: 'Yoga positions', options: ['Wheel', 'Cobra', 'Lotus', 'Diamond']},
    group_3: {connection: 'Constellations', options: ['Orion', 'Crater', 'Indus', 'Hydra']},
    group_4: {connection: 'Snow __', options: ['Board', 'Ball', 'Mobile', 'Plough']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Magic __', options: ['Bullet', 'Carpet', 'Circle', 'Lantern']},
    group_2: {connection: 'Ways to criticise', options: ['Roast', 'Slate', 'Pan', 'Hammer']},
    group_3: {connection: 'Waveforms', options: ['Triangle', 'Square', 'Sine', 'Sawtooth']},
    group_4: {connection: 'Famous Peters', options: ['Robinson', 'Powell', 'Cook', 'Jackson']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Famous Francises', options: ['Chichester', 'Crick', 'Poulenc', 'Fukuyama']},
    group_2: {connection: 'Literary quartets', options: ['Frederica', 'Rabbit', 'Raj', 'Twilight']},
    group_3: {connection: 'Cricket terms', options: ['Cherry', 'Duck', 'Bunny', 'Nelson']},
    group_4: {connection: 'Things that can be smoked', options: ['Salmon', 'Cigar', 'Glass', 'Bacon']},
    //: 'Series 2'
  },
  {
    group_1: {connection: 'Currency nicknames', options: ['Loonie', 'Kiwi', 'Cable', 'Buck']},
    group_2: {connection: 'Fictional princesses', options: ['Ariel', 'Peach', 'Wonder Woman', 'Sara Crewe']},
    group_3: {connection: 'Sky __', options: ['Light', 'Rocket', 'Lark', 'Clad']},
    group_4: {connection: 'Steam engines', options: ['Tornado', 'Iron Duke', 'City of Truro', 'Mallard']},
    //: 'Series 2'
  },
  //==================== series 3 ==================
  {
    group_1: {connection: 'Used to describe horse coats', options: ['Chestnut', 'Roan', 'Grey', 'Bay']},
    group_2: {connection: 'Man made satellites', options: ['Sputnik', 'Asterix', 'Astra', 'Big Bird']},
    group_3: {connection: 'Cricket shots', options: ['Hook', 'Sweep', 'Square cut', 'Block']},
    group_4: {connection: 'Satirical publications', options: ['Spy', 'Private Eye', 'Punch', 'Onion']},
    //: 'Series 3'
  },
  {
    group_1: {connection: 'British comics', options: ['Buster', 'Hotspur', 'Viz', 'Twinkle']},
    group_2: {connection: 'Cut the __', options: ['Mustard', 'Deck', 'Ribbon', 'Gordian knot']},
    group_3: {connection: 'Lunar modules', options: ['Spider', 'Falcon', 'Snoopy', 'Eagle']},
    group_4: {connection: 'Celebrity nicknames', options: ['Hitch', 'Bogey', 'Dino', 'Madge']},
    //: 'Series 3'
  },
  {
    group_1: {connection: 'Actors in "Dads Army"', options: ['Beck', 'Pertwee', 'Ridley', 'Lowe']},
    group_2: {connection: 'England goalkeepers', options: ['Robinson', 'Seaman', 'Banks', 'James']},
    group_3: {connection: 'Merchant __', options: ['Tailor', 'Prince', 'Bank', 'Navy']},
    group_4: {connection: 'Shades of blue', options: ['Lavender', 'Midnight', 'Oxford', 'Royal']},
    //: 'Series 3'
  },
  {
    group_1: {connection: 'Shades of green', options: ['Racing', 'Olive', 'Bottle', 'Lime']},
    group_2: {connection: 'US assassinated figures', options: ['Milk', 'Garfield', 'King', 'Lincoln']},
    group_3: {connection: 'Types of maid', options: ['House', 'Nurse', 'Bar', 'Hand']},
    group_4: {connection: 'Famous violinists', options: ['Kennedy', 'Bell', 'Stern', 'Little']},
    //: 'Series 3'
  },
  {
    group_1: {connection: 'Famous people called Gordon', options: ['Ramsay', 'Banks', 'Burn', 'Brown']},
    group_2: {connection: 'Straw __', options: ['Mushroom', 'Bale', 'Man', 'Hat']},
    group_3: {connection: 'Palaces', options: ['Crystal', 'Louvre', 'Lambeth', 'Lateran']},
    group_4: {connection: 'Premiership football referees', options: ['Bennett', 'Winter', 'Poll', 'Dean']},
    //: 'Series 3'
  },
  {
    group_1: {connection: 'Male animals', options: ['Buck', 'Bull', 'Stag', 'Tom']},
    group_2: {connection: '__ tooth', options: ['Dog', 'Milk', 'Eye', 'Sabre']},
    group_3: {connection: 'Famous people called Norman', options: ['Cook', 'Rockwell', 'Parkinson', 'Wisdom']},
    group_4: {connection: 'British photographers', options: ['Bailey', 'Beaton', 'Donovan', "O'Neill"]},
    //: 'Series 3'
  },
  {
    group_1: {connection: "Symbols on a fruit machine", options: ["Bar", "Lemon", "7", "Bell"]},
    group_2: {connection: "End with a silent letter", options: ["Crumb", "Column", "Debris", "Crochet"]},
    group_3: {connection: "Statistical charts", options: ["Radar", "Pie", "Scatter", "Line"]},
    group_4: {connection: "British cartoonists", options: ["Tidy", "Heath", "Giles", "Simmonds"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "__ theory", options: ["Big Bang", "Chaos", "String", "Game"]},
    group_2: {connection: "Non-animal farms", options: ["Health", "Funny", "Wind", "Body"]},
    group_3: {connection: "Terms used to describe wine", options: ["Nose", "Legs", "Finish", "Bouquet"]},
    group_4: {connection: "Second __", options: ["Coming", "Hand", "Childhood", "Guess"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Egyptian deities", options: ["Nut", "Bat", "Set", "Isis"]},
    group_2: {connection: "Things you can crack", options: ["Whip", "Smile", "Joke", "Bottle"]},
    group_3: {connection: "Necks of jumpers", options: ["Turtle", "Polo", "Roll", "Crew"]},
    group_4: {connection: "Implements in ball sports", options: ["Stick", "Club", "Mallet", "Cue"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Joined together by links", options: ["Sausages", "Chain", "Cuff", "Web pages"]},
    group_2: {connection: "Characters in 'Punch and Judy'", options: ["Joey", "Policeman", "Hangman", "Crocodile"]},
    group_3: {connection: "Types of jug", options: ["Puzzle", "Claret", "Measuring", "Toby"]},
    group_4: {connection: "Sizes of grand piano", options: ["Baby", "Concert", "Ballroom", "Parlour"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Flightless birds", options: ["Kiwi", "Penguin", "Kakapo", "Rhea"]},
    group_2: {connection: "Booker Prize winning authors", options: ["Swift", "Barker", "Doyle", "Amis"]},
    group_3: {connection: "Synonyms for swindle", options: ["Screw", "Fleece", "Con", "Rook"]},
    group_4: {connection: "__ walk", options: ["Cat", "Sleep", "Moon", "Cake"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Terms of endearment", options: ["Petal", "Love", "Precious", "Honey"]},
    group_2: {connection: "Architects of London buildings", options: ["Barry", "Wren", "Nash", "Hawksmoor"]},
    group_3: {connection: "__ metal", options: ["Scrap", "Noble", "Base", "Heavy"]},
    group_4: {connection: "Famous people called Grace", options: ["Jones", "Kelly", "Darling", "Slick"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Characters in 'Just William'", options: ["Henry", "William", "Douglas", "Jumble"]},
    group_2: {connection: "Famous Spencers", options: ["Davis", "Tracy", "Tunick", "Perceval"]},
    group_3: {connection: "Fictional islands", options: ["Amity", "Skull", "Kirrin", "Craggy"]},
    group_4: {connection: "TV production companies", options: ["Avalon", "Ragdoll", "Ginger", "Hat Trick"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Spices", options: ["Saffron", "Mace", "Nigella", "Ginger"]},
    group_2: {connection: "Pink __", options: ["Gin", "Panther", "Lady", "Floyd"]},
    group_3: {connection: "Fictional streets", options: ["Elm", "Ramsay", "Bash", "Sesame"]},
    group_4: {connection: "TV chefs", options: ["Rhodes", "Blanc", "Oliver", "Rankin"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Public schools", options: ["St Pauls", "Repton", "Charterhouse", "Merchant Taylors"]},
    group_2: {connection: "Westminster __", options: ["Cathedral", "Village", "Abbey", "Chimes"]},
    group_3: {connection: "Corners of Silverstone", options: ["Becketts", "Brooklands", "Copse", "Stowe"]},
    group_4: {connection: "London livery companies", options: ["Salters", "Fishmongers", "Vintners", "Skinners"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "London lanes", options: ["Brick", "Pudding", "Chancery", "Park"]},
    group_2: {connection: "Terms used in bowls", options: ["Jack", "Ditch", "Wood", "Rink"]},
    group_3: {connection: "English hymn writers", options: ["Cowper", "Newton", "Wesley", "Watts"]},
    group_4: {connection: "Names of humorous laws", options: ["Murphy", "Parkinson", "Gresham", "Sturgeon"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "'Test Match Special' regulars", options: ["Agnew", "Blofeld", "Boycott", "Johnston"]},
    group_2: {connection: "On your __", options: ["Knees", "Bike", "Last legs", "Marks"]},
    group_3: {connection: "Words originating from Irish", options: ["Banshees", "Tory", "Brock", "Galore"]},
    group_4: {connection: "Last letter to front creates a new word", options: ["Angled", "Uppers", "Elating", "Eighth"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Famous riots", options: ["Brixton", "Poll Tax", "Rodney King", "Gordon"]},
    group_2: {connection: "Ends of Test Cricket grounds", options: ["Vauxhall", "Pavilion", "City", "Nursery"]},
    group_3: {connection: "Star clusters", options: ["Seven Sisters", "Beehive", "Hyades", "Trapezium"]},
    group_4: {connection: "Places in Ealing Studios film titles", options: ["Lavender Hill", "Antarctic", "Titfield", "Pimlico"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Typographical symbols", options: ["Hash", "Pilcrow", "Bullet", "Dagger"]},
    group_2: {connection: "Parts of a chimney", options: ["Pot", "Stack", "Breast", "Piece"]},
    group_3: {connection: "Dashing young men", options: ["Dandy", "Blade", "Blood", "Rake"]},
    group_4: {connection: "All have teeth", options: ["Comb", "Zip", "Gearwheel", "Saw"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "__ question", options: ["Irish", "$64,000", "Trick", "West Lothian"]},
    group_2: {connection: "Bricklaying bonds", options: ["Flemish", "English", "Rat-trap", "Header"]},
    group_3: {connection: "Types of snooker rest", options: ["Butt", "Cross", "Swan neck", "Spider"]},
    group_4: {connection: "White space on a page", options: ["Alley", "Leading", "Gutter", "Margin"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Baseball pitches", options: ["Fast", "Curve", "Spit", "Screw"]},
    group_2: {connection: "Characters called Mr. __", options: ["Rochester", "Potato Head", "Bean", "Benn"]},
    group_3: {connection: "Types of sole", options: ["Torbay", "Dover", "Rock", "Lemon"]},
    group_4: {connection: "Mathematical theorems", options: ["Sandwich", "Four Colour", "Hairy Ball", "Binomial"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Things governed by Laws", options: ["Motion", "Nature", "Robotics", "Attraction"]},
    group_2: {connection: "US state capitals", options: ["Olympia", "Bismarck", "Phoenix", "Austin"]},
    group_3: {connection: "Welsh female singers", options: ["Matthews", "Duffy", "Jenkins", "Church"]},
    group_4: {connection: "Beau __", options: ["Nash", "Peep", "Bridges", "Geste"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "British record labels", options: ["Virgin", "Island", "London", "Factory"]},
    group_2: {connection: "__ monkey", options: ["Cheeky", "Code", "Brass", "Powder"]},
    group_3: {connection: "Homophones of countries", options: ["Chilly", "Whales", "Grease", "Columbia"]},
    group_4: {connection: "Cold conditions", options: ["Crisp", "Gelid", "Keen", "Parky"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Famous Montys", options: ["Norman", "Don", "Burns", "Panesar"]},
    group_2: {connection: "__ monitor", options: ["Milk", "Heart", "Computer", "Baby"]},
    group_3: {connection: "Computer programming languages", options: ["Python", "Logo", "Perl", "C"]},
    group_4: {connection: "Types of coffee", options: ["Bourbon", "K7", "Blue Mountain", "Java"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Adopted", options: ["Moses", "Kate Adie", "Cabbage Patch Kids", "Superman"]},
    group_2: {connection: "__ basket", options: ["Bread", "Fruit", "Picnic", "Linen"]},
    group_3: {connection: "Repeated in song titles", options: ["Sugar", "New York", "Louie", "Rebel"]},
    group_4: {connection: "Famous Dickies", options: ["Henderson", "Davies", "Valentine", "Bird"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Breeds of terrier", options: ["Fox", "Border", "Boston", "Norfolk"]},
    group_2: {connection: "Names meaning 'red'", options: ["Russell", "Rufus", "Rory", "Phoenix"]},
    group_3: {connection: "Surnames of 'Sex and the City' women", options: ["York", "Jones", "Hobbes", "Bradshaw"]},
    group_4: {connection: "Blood __", options: ["Stock", "Hound", "Line", "Bath"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "Second halves of double acts", options: ["Hardy", "Pace", "Large", "Bracket"]},
    group_2: {connection: "Orchestra leaders", options: ["Miller", "May", "Whiteman", "Heath"]},
    group_3: {connection: "__ ball", options: ["Crystal", "Masked", "Medicine", "Demolition"]},
    group_4: {connection: "Patience card games", options: ["Spider", "Golf", "Klondike", "Clock"]},
    //: "Series 3"
  },
  {
    group_1: {connection: "'Are You Being Served?' characters", options: ["Brahms", "Peacock", "Harman", "Grace"]},
    group_2: {connection: "Famous threes", options: ["Unity", "Degree", "Stooge", "R"]},
    group_3: {connection: "Deputy Leaders of the Labour Party", options: ["Jenkins", "Healy", "Beckett", "Morrison"]},
    group_4: {connection: "Butterflies", options: ["Brimstone", "Monarch", "Skipper", "Comma"]},
    //: "Series 3"
  },
  // wall below is from series 18 episode 28, too replace a non family friendly wall !
  {
      group_1: {connection: "US President assassination years", options: ["1865", "1881", "1901", "1963"]},
      group_2: {connection: "Films with number titles", options: ["10", "300", "1917", "2012"]},
      group_3: {connection: "Palindromes", options: ["343", "404", "1771", "2002"]},
      group_4: {connection: "Square numbers in binary", options: ["1", "100", "1001", "10000"]}
  },
  {
    group_1: {connection: "Vehicles in 'Bob the Builder'", options: ["Dizzy", "Muck", "Lofty", "Scoop"]},
    group_2: {connection: "Major varieties of orange", options: ["Persian", "Blood", "Valencia", "Navel"]},
    group_3: {connection: "End in girls names", options: ["Chamber", "Chalice", "Retina", "Senora"]},
    group_4: {connection: "Works by Lord Byron", options: ["Cain", "Beppo", "Darkness", "Don Juan"]},
    //: "Series 3"
  },
  //////////////////// next is series 4 episode 1 ////////////////////////
  {
    group_1: {connection: "End in 4 vowels", options: ["Louie", "Sequoia", "Onomat -opoeia", "Pacquiao"]},
    group_2: {connection: "Things you can metaphorically 'jump'", options: ["Broomstick", "Queue", "Gun", "Shark"]},
    group_3: {connection: "Countries called __land", options: ["Ice", "Po", "Ire", "Fin"]},
    group_4: {connection: "First Presidents", options: ["Yeltsin", "Banana", "Kenyatta", "Washington"]},
  },
  {
    group_1: {connection: "David Jason roles", options: ["Danger Mouse", "Blanco", "Frost", "Granville"]},
    group_2: {connection: "Wore an eye patch", options: ["Largo", "Moshe Dayan", "Gabrielle", "Rooster Cogburn"]},
    group_3: {connection: "Words meaning 'quick'", options: ["Zippy", "Speedy", "Pronto", "Presto"]},
    group_4: {connection: "Cricketing nicknames", options: ["Punter", "King of Spain", "White Lightning", "The Wall"]},
  },
  {
    group_1: {connection: "Disney ducks", options: ["Daisy", "Dewey", "Donald", "Louie"]},
    group_2: {connection: "British fashion designers", options: ["Westwood", "McCartney", "Guiness", "Hemingway"]},
    group_3: {connection: "__ circle", options: ["Magic", "Vicious", "Dress", "Crop"]},
    group_4: {connection: "Final letter can be accented", options: ["Pique", "Rose", "Expose", "Lame"]},
  },
  {
    group_1: {connection: "Loans", options: ["Tick", "HP", "Credit", "Never Never"]},
    group_2: {connection: "Counties of New York City", options: ["Queens", "Bronx", "New York", "Richmond"]},
    group_3: {connection: "Types of sauce", options: ["Cheese", "Mint", "Bread", "Buffalo"]},
    group_4: {connection: "__ dust", options: ["Fairy", "Gold", "Angel", "Chalk"]},
  },
  {
    group_1: {connection: "The Great __", options: ["Dictator", "Depression", "Bear", "Unwashed"]},
    group_2: {connection: "Collective nouns for birds", options: ["Skein", "Murder", "Charm", "Parliament"]},
    group_3: {connection: "Types of tank", options: ["Sheridan", "Sherman", "Challenger", "Centurion"]},
    group_4: {connection: "Things that have stripes", options: ["US flag", "Sergeant", "Barber's pole", "Tiger"]},
  },
  {
    group_1: {connection: "Characters in 'Madame Butterfly'", options: ["Pinkerton", "Cio-Cio San", "Suzuki", "Goro"]},
    group_2: {connection: "Spin-offs", options: ["Lewis", "Angel", "Joey", "Frasier"]},
    group_3: {connection: "Types of ink", options: ["Tattoo", "Invisible", "Indian", "Squid"]},
    group_4: {connection: "'Mortal Kombat' fighters", options: ["Sub-Zero", "Reptile", "Scorpion", "Liu Kang"]},
  },
  {
    group_1: {connection: "PC computer keys", options: ["Insert", "Escape", "Function", "Control"]},
    group_2: {connection: "UK Cabinet Secretaries", options: ["Home", "Defence", "Justice", "Scottish"]},
    group_3: {connection: "__ shock", options: ["Toxic", "Electric", "Future", "Culture"]},
    group_4: {connection: "Stages at Glastonbury", options: ["John Peel", "Acoustic", "Pyramid", "Other"]},
  },
  {
    group_1: {connection: "Types of hat", options: ["Derby", "Pork pie", "Busby", "Toque"]},
    group_2: {connection: "Advertising characters", options: ["Vinnie", "Aleksandr Orlov", "Nipper", "Bibendum"]},
    group_3: {connection: "Woodworking joints", options: ["Butt", "Dovetail", "Mitre", "Finger"]},
    group_4: {connection: "Accoutrements for a snowman", options: ["Coal", "Pipe", "Scarf", "Carrot"]},
  },
  {
    group_1: {connection: "__ arts", options: ["Dark", "Fine", "Beaux", "Martial"]},
    group_2: {connection: "Things that have bells", options: ["Jester", "Morris dancer", "Sleigh", "Swiss cow"]},
    group_3: {connection: "Camera shots", options: ["Cutaway", "Noddy", "Wide", "Talking head"]},
    group_4: {connection: "Taxi drivers", options: ["Edmonds", "Bickle", "Housego", "Fry"]},
  },
  {
    group_1: {connection: "Streets in New York City", options: ["Wall", "42nd", "Lafayette", "Canal"]},
    group_2: {connection: "Famous people called Max", options: ["Ernst", "Hastings", "Clifford", "Miller"]},
    group_3: {connection: "Go to the __", options: ["Dogs", "Country", "Wire", "Top of the class"]},
    group_4: {connection: "19th century battles", options: ["Sedan", "Gettysburg", "Balaclava", "Waterloo"]},
  },
  {
    group_1: {connection: "Things that go on teeth", options: ["Cap", "Crown", "Bridge", "Veneer"]},
    group_2: {connection: "Places in Ukraine", options: ["Balaclava", "Odessa", "Keiv", "Yalta"]},
    group_3: {connection: "Michael Frayn plays", options: ["Democracy", "Benefactors", "Noises Off", "Copenhagen"]},
    group_4: {connection: "Military horses", options: ["Bucephalus", "Nelson", "Traveller", "Marengo"]},
  },
  {
    group_1: {connection: "Members of Queen", options: ["Taylor", "Mercury", "May", "Deacon"]},
    group_2: {connection: "Ford vehicle models", options: ["Zodiac", "Prefect", "Transit", "Scorpio"]},
    group_3: {connection: "Religious offices", options: ["Pastor", "Cantor", "Priest", "Bishop"]},
    group_4: {connection: "Classes of mathematical series", options: ["Power", "Harmonic", "Fourier", "Telescoping"]},
  },
  {
    group_1: {connection: "Pronounced as 2 letters", options: ["Okay", "Eighty", "Tepee", "Excess"]},
    group_2: {connection: "Things with poisonous varieties", options: ["Mushroom", "Jellyfish", "Snake", "Algae"]},
    group_3: {connection: "Types of vines", options: ["Clematis", "Wisteria", "Grape", "Ivy"]},
    group_4: {connection: "Words of Native American origin", options: ["Tomato", "Mesquite", "Toboggan", "Kayak"]},
  },
  {
    group_1: {connection: "Turnovers", options: ["Strudel", "Knish", "Bridie", "Calzone"]},
    group_2: {connection: "Disney animated villains", options: ["Scar", "Ursula", "Stromboli", "Hades"]},
    group_3: {connection: "'Old' names for the Devil", options: ["Ned", "Scratch", "Nick", "Harry"]},
    group_4: {connection: "Nicknames for women", options: ["Doll", "Bird", "Dame", "Sheila"]},
  },
  {
    group_1: {connection: "__ eel", options: ["Electric", "Moray", "Conger", "Sand"]},
    group_2: {connection: "Famous New Zealanders", options: ["Campion", "Hillary", "Rutherford", "Finn"]},
    group_3: {connection: "Test wicketkeepers", options: ["Murray", "Prior", "Engineer", "Gilchrist"]},
    group_4: {connection: "Historic counties of Scotland", options: ["Angus", "Sutherland", "Fife", "Argyll"]},
  },
  {
    group_1: {connection: "Americans called Jerry", options: ["Lewis", "Hall", "Garcia", "Springer"]},
    group_2: {connection: "Islands around the British Isles", options: ["Yell", "Jersey", "Hoy", "Mull"]},
    group_3: {connection: "Breeds of spaniel", options: ["Cocker", "Field", "King Charles", "Sussex"]},
    group_4: {connection: "Magnetic __", options: ["Ink", "North", "Flux", "Storm"]},
  },
  {
    group_1: {connection: "Liberal Democrat leaders", options: ["Steel", "Cable", "Cambell", "Kennedy"]},
    group_2: {connection: "Submachine guns", options: ["Browning", "Owen", "Lanchester", "Thompson"]},
    group_3: {connection: "Members of Take That", options: ["Orange", "Donald", "Barlow", "Williams"]},
    group_4: {connection: "Types of Pokemon", options: ["Fighting", "Grass", "Dragon", "Psychic"]},
  },
  {
    group_1: {connection: "Haydn symphonies", options: ["Clock", "Surprise", "Military", "London"]},
    group_2: {connection: "Parts of a typewriter", options: ["Platen", "Ribbon", "Key", "Carriage"]},
    group_3: {connection: "Clocks", options: ["Alarm", "Cuckoo", "Candle", "Chess"]},
    group_4: {connection: "Things that have eyes", options: ["Potato", "Storm", "Needle", "Jupiter"]},
  },
  {
    group_1: {connection: "Wore bowler hats", options: ["Mr Benn", "Alex Delarge", "Oddjob", "The Riddler"]},
    group_2: {connection: "Nicknamed 'The Red __'", options: ["Von Richtofen", "Vivaldi", "Mars", "Manchester United"]},
    group_3: {connection: "Cockerel logos", options: ["Kellog's", "Tottenham Hotspur", "Pathe", "France"]},
    group_4: {connection: "Last letter removed = a currency", options: ["Realm", "Randy", "Dinara", "Levi"]},
  },
  {
    group_1: {connection: "Blades", options: ["Helicopter", "Razor", "Sheffield United", "Ice skate"]},
    group_2: {connection: "Internet top level domains", options: ["Museum", "Org", "Biz", "Gov"]},
    group_3: {connection: "Galaxies", options: ["Milky Way", "Cigar", "Pinwheel", "Andromeda"]},
    group_4: {connection: "Things that can be drawn", options: ["Blood", "Cart", "Curtain", "Sword"]},
  },
  // next is series 4 episode 11
]
